package com.twitter.sdk.android.tweetui;

import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.GuestCallback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.models.Tweet;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Luiz Carlos do Lago on 28/04/16.
 */
public class HomeTimeLine extends BaseTimeline implements Timeline<Tweet> {

    private static final String SCRIBE_SECTION = "home";
    final Integer count;
    final Boolean trimUser;
    final Boolean excludeReplies;
    final Boolean contributorDetails;
    final Boolean includeEntities;

    HomeTimeLine(TweetUi tweetUi, Integer count, Boolean trimUser, Boolean excludeReplies, Boolean contributorDetails, Boolean includeEntities) {
        super(tweetUi);
        this.count = count;
        this.trimUser = trimUser;
        this.excludeReplies = excludeReplies;
        this.contributorDetails = contributorDetails;
        this.includeEntities = includeEntities;
    }

    @Override
    String getTimelineType() {
        return SCRIBE_SECTION;
    }

    @Override
    public void next(Long sinceId, Callback<TimelineResult<Tweet>> cb) {
        this.addRequest(this.createHomeTimeLineRequest(sinceId, (Long)null, cb));
    }

    @Override
    public void previous(Long maxId, Callback<TimelineResult<Tweet>> cb) {
        this.addRequest(this.createHomeTimeLineRequest((Long)null, decrementMaxId(maxId), cb));
    }

    Callback<TwitterApiClient> createHomeTimeLineRequest(final Long sinceId, final Long maxId, final Callback<TimelineResult<Tweet>> cb) {
        return new LoggingCallback<TwitterApiClient>(cb, Fabric.getLogger()) {
            @Override
            public void success(Result<TwitterApiClient> result) {
                ((TwitterApiClient)result.data).getStatusesService().homeTimeline(
                        count, sinceId, maxId, trimUser, excludeReplies, contributorDetails, includeEntities, new GuestCallback(new TweetsCallback(cb))
                );
            }
        };
    }

    public static class Builder {
        private final TweetUi tweetUi;

        private Integer count;
        private Boolean trimUser;
        private Boolean excludeReplies;
        private Boolean contributorDetails;
        private Boolean includeEntities;


        public Builder() {
            this(TweetUi.getInstance());
        }

        public Builder(TweetUi tweetUi) {
            if(tweetUi == null) {
                throw new IllegalArgumentException("TweetUi instance must not be null");
            } else {
                this.tweetUi = tweetUi;
            }
        }

        public HomeTimeLine.Builder count(Integer count) {
            this.count = count;
            return this;
        }

        public HomeTimeLine.Builder trimUser(Boolean trimUser) {
            this.trimUser = trimUser;
            return this;
        }

        public HomeTimeLine.Builder excludeReplies(Boolean excludeReplies) {
            this.excludeReplies = excludeReplies;
            return this;
        }

        public HomeTimeLine.Builder contributorDetails(Boolean contributorDetails) {
            this.contributorDetails = contributorDetails;
            return this;
        }

        public HomeTimeLine.Builder includeEntities(Boolean includeEntities) {
            this.includeEntities = includeEntities;
            return this;
        }

        public HomeTimeLine build() {
            return new HomeTimeLine(this.tweetUi, this.count, this.trimUser, this.excludeReplies, this.contributorDetails, this.includeEntities);
        }
    }

}
