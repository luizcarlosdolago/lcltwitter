package br.com.lcltwitter.lcltwitter;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.tweetui.HomeTimeLine;
import com.twitter.sdk.android.tweetui.TimelineResult;
import com.twitter.sdk.android.tweetui.TweetTimelineListAdapter;

public class TimelineViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timeline_view);
        mappingViews();
    }

    public void mappingViews() {

        String dados = getIntent().getStringExtra( Constantes.PARAM_TWITTER_SESSION );
        TwitterSession session = new Gson().fromJson( dados, TwitterSession.class );

        final HomeTimeLine homeTimeLine = new HomeTimeLine.Builder().build();

        final TweetTimelineListAdapter adapter = new TweetTimelineListAdapter.Builder(this)
                .setTimeline(homeTimeLine)
                .build();

        TextView emptyText = (TextView) findViewById(android.R.id.empty);
        final ListView tweetList = (ListView)findViewById(android.R.id.list);
        final SwipeRefreshLayout swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);

        tweetList.setEmptyView(emptyText);
        tweetList.setAdapter(adapter);

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeContainer.setRefreshing(true);
                adapter.refresh(new Callback<TimelineResult<Tweet>>() {
                    @Override
                    public void success(Result<TimelineResult<Tweet>> result) {
                        swipeContainer.setRefreshing(false);
                    }

                    @Override
                    public void failure(TwitterException exception) {
                    }
                });
            }
        });

        tweetList.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int t) {
                if(firstVisibleItem == 0 && isTopTheList(tweetList)){
                    swipeContainer.setEnabled(true);
                }else{
                    swipeContainer.setEnabled(false);
                }
            }
        });
    }

    public boolean isTopTheList( ListView list ) {
        if(list.getChildCount() == 0) return true;
        return list.getChildAt(0).getTop() == 0;
    }

}
